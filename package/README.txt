ABOUT:
This is a mod for Particle Fleet: Emergence

This mod does 3 things:
-Map screenshots (F11 by default) are now 4x the resolution
-Map screenshots now hide all Text and ResourceBar while taking the screenshots
-Screenshots are now in PNG format (increases both file size and quality)

INSTALLATION:

Unpack this zip into Particle Fleet Emergence\ParticleFleet_Data\Managed and run patch.bat to install.

NOTE:
To uninstall the mod, delete the file Assembly-CSharp.dll and replace it with Assembly-CSharp.backup.dll
Do not run the patch.bat again after installing or you might lose the original backup
If you do lose the original file and want to revert the changes, right-click Particle Fleet in your library, open Preferences, navigate to Local Files and press "Verify integrity of game files...". The game will check all files and redownload the original versions of any files that have been modified.