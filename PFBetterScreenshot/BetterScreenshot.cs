﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

internal class patch_GameSpace : GameSpace
{
    public extern Texture2D orig_GetScreenShotTexture(bool fullMap, float zoom, bool preview);
    public Texture2D GetScreenShotTexture(bool fullMap, float zoom, bool preview)
    {
        if (fullMap && zoom == 1f)
        {
            zoom = 4f;

            Text[] texts = UnityEngine.Object.FindObjectsOfType<Text>();
            Color[] text_colors = new Color[texts.Length];

            ResourceBar[] bars = UnityEngine.Object.FindObjectsOfType<ResourceBar>();
            Color[] bar_colors = new Color[texts.Length];

            Color rep = new Color(1, 1, 1, 0);

            for (int i = 0; i < bars.Length; i++)
            {
                bar_colors[i] = bars[i].barColor;
                bars[i].barColor = rep;
                bars[i].RefreshColors();
            }

            for (int i = 0; i<texts.Length; i++)
            {
                text_colors[i] = texts[i].color;
                texts[i].color = rep;
            }

            var ret = orig_GetScreenShotTexture(fullMap, zoom, preview);

            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].color = text_colors[i];
            }

            for (int i = 0; i < bars.Length; i++)
            {
                bars[i].barColor = bar_colors[i];
                bars[i].RefreshColors();
            }

            return ret;
        }
        else
            return orig_GetScreenShotTexture(fullMap, zoom, preview);
    }

    public void TakeScreenShot(bool fullMap)
	{
		Texture2D screenShotTexture = this.GetScreenShotTexture(fullMap, 1f);
		if (screenShotTexture != null)
		{
            Texture2D newTexture = new Texture2D(screenShotTexture.width, screenShotTexture.height, TextureFormat.ARGB32, false);
            newTexture.SetPixels(0, 0, screenShotTexture.width, screenShotTexture.height, screenShotTexture.GetPixels());
            newTexture.Apply();
            byte[] bytes = newTexture.EncodeToPNG();
            UnityEngine.Object.DestroyImmediate(screenShotTexture);
			if (bytes != null)
			{
				File.WriteAllBytes(FileManager.GetScreenShotFileName(), bytes);
			}
			else
			{
				Debug.Log("Null Screenshot data");
			}
		}
	}
}

internal class patch_FileManager : FileManager
{
    public static string GetScreenShotFileName()
    {
        string path = FileManager.GetBaseDataDir() + "screenshots/";
        Directory.CreateDirectory(path);
        string[] files = Directory.GetFiles(path, "*.png");
        int num = 0;
        string[] array = files;
        for (int i = 0; i < array.Length; i++)
        {
            string path2 = array[i];
            try
            {
                string[] array2 = Path.GetFileName(path2).Split(new char[]
                {
                '.'
                });
                if (array2.Length > 0)
                {
                    string[] array3 = array2[0].Split(new char[]
                    {
                    '_'
                    });
                    if (array3.Length > 1)
                    {
                        int num2 = int.Parse(array3[1]);
                        if (num2 > num)
                        {
                            num = num2;
                        }
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        string text = string.Concat(new object[]
        {
        FileManager.GetBaseDataDir(),
        "screenshots/pfss_",
        num + 1,
        ".png"
        });
        Debug.Log(text);
        return text;
    }

}